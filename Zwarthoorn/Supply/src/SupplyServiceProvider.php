<?php 
namespace Zwarthoorn\Supply;


use Illuminate\Support\ServiceProvider;


class SupplyServiceProvider extends ServiceProvider {

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/Http/routes.php';
        $this->loadViewsFrom(__DIR__.'/views', 'Coreviews');
        $this->loadViewsFrom(__DIR__.'/views/sentinel/layouts', 'Outlay');
        $this->publishes([
            __DIR__.'/views' => base_path('resources/views'),
            __DIR__.'/config/supply.php' => config_path('supply.php'),
            __DIR__.'/migrations' => database_path('/migrations')
        ]);
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register('Sentinel\SentinelServiceProvider');
       // $this->app->register('Zwarthoorn\Supply\Controllers\SupplyController');
    }

}