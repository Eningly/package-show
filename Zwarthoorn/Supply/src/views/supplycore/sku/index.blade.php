@extends(config('supply.layout'))

{{-- Web site Title --}}
@section('title')
@parent
Sku
@stop

{{-- Content --}}
@section('content')
<div class="row">
    <div class='page-header'>
        <div class='btn-toolbar pull-right'>
            <div class='btn-group'>
                <a class='btn btn-primary' href="{{route('supply.sku.create')}}">Create Sku</a>
            </div>
        </div>
        <h1>Available Sku's</h1>
    </div>
</div>

<div class="row">
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
                <th>Name</th>
                <th>Options</th>
            </thead>
            <tbody>
                @foreach($skus as $sku)
                    <td>{{$sku->name}}</td>
                    <td><button class="btn btn-default" onClick="location.href="{{route('supply.sku.edit')}}{{$sku->id}}">Edit</button></td>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@stop

