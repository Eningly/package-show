@extends(config('sentinel.layout'))

{{-- Web site Title --}}
@section('title')
@parent
Create Sku
@stop

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <form method="POST" action="{{ route('supply.sku.store') }}" accept-charset="UTF-8">

            <h2>Create New Sku</h2>

            <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                <input class="form-control" placeholder="Name" name="name" type="text"  value="{{ Request::old('name') }}">
                {{ ($errors->has('name') ? $errors->first('name') : '') }}
            </div>

            

            <input name="_token" value="{{ csrf_token() }}" type="hidden">
            <input class="btn btn-primary" value="Create Sku" type="submit">

        </form>

    </div>
</div>

@stop