<?php

namespace Zwarthoorn\Supply\Models;

use Illuminate\Database\Eloquent\Model;

class Sku extends Model
{
    protected $table = 'sku';
    protected $fillable = ['name'];
}
