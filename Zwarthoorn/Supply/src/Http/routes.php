<?php 


/*
|--------------------------------------------------------------------------
| Supply Core Routes.
|--------------------------------------------------------------------------
|
|	Here you can find all the core Supply package routes and add to them if you need to.
|
*/


Route::group(['namespace' => 'Zwarthoorn\Supply\Controllers','middleware' => ['web']], function () {

	Route::group(['middleware' => ['has.permission:admin']],function ()
	{
		Route::get('/',['as'=>'home','uses'=>'SupplyController@index']);
		Route::get('/sku',['as'=>'supply.sku','uses'=>'SkuController@index']);
		Route::get('/sku/create',['as'=>'supply.sku.create','uses'=>'SkuController@create']);
		Route::post('/sku/store',['as'=>'supply.sku.store','uses'=>'SkuController@store']);
	});

});