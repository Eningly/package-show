<?php

namespace Zwarthoorn\Supply\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Zwarthoorn\Supply\Models\Sku;

class SkuController extends Controller
{
    public function index()
    {
    	
    	return view('Coreviews::supplycore.sku.index',['skus'=>Sku::all()]);
    }
    public function create()
    {
    	return view('Coreviews::supplycore.sku.create');
    }
    public function store(Request $request)
    {
    	 $this->validate($request, [
        	'name' => "required|max:255|regex:/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u"
    	]);

    	$sku = Sku::create(['name'=>$request->name]);
    	return redirect('/sku');
    }
}
